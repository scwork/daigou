package com.daigou.dao;

import java.util.List;

import com.daigou.dto.UserQuery;
import com.daigou.model.User;

public interface UserMapper{
	/**
	 * 登录
	 * @param loginName
	 * @param pwd
	 * @return
	 */
   public User login(String loginName,String pwd);
   /**
    * 注册
    * @param userInfo
    * @return
    */
   public int register(User userInfo);
   /**
    * 增加员工
    * @param userInfo
    * @return
    */
   public int insertEmployee(User employee);
   /**
    * 通过主键查找用户id
    * @param uId
    * @return
    */
   public User selectByPrimaryKey(Long id);
   
   /**
    * 通过shopId查找管理员
    * @param shopId
    * @return
    */
   public User selectAdminUser(Long shopId);
   
   /**
    * 通过loginName查找用户id
    * @param uId
    * @return
    */
   public User selectByLoingName(String loginName);
   /**
    * 通过email查找用户id
    * @param uId
    * @return
    */
   public User selectByEmail(String email);
   /**
    * 通过邮箱重置密码
    * @param email
    * @param pwd
    * @return
    */
   public int updatePasswordByEmail(String email,String pwd);
   /**
    * 通过id重置密码
    * @param email
    * @param pwd
    * @return
    */
   public int updatePasswordById(Long id,String pwd);
   /**
    * 查找所有用户
    * @return
    */
   public List<User> selectAllUser();
   /**
    * 修改用户信息
    * @param userInfo
    * @return
    */
   public int updateByPrimaryKeySelective(User userInfo);/**
	 * 从订单汇总我所有的客户
	 * @param userId
	 * @return
	 */
	public List<User> selectMyEmployee(UserQuery userQuery);
}