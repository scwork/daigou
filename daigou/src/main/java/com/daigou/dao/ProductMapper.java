package com.daigou.dao;

import java.util.List;
import java.util.Map;

import com.daigou.dto.ProductQuery;
import com.daigou.model.Product;

public interface ProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Long id);
    
    List<Product> selectByIds(String ids);
    
    List<Product> selectByShopId(Map<String,Object> paramMap);
    
    List<Product> selectByProductByPage(ProductQuery productQuery);
    
    List<Product> selectCaogaoByShopId(Map<String,Object> paramMap);
    /**
     * 查上架总数（不分页）
     * @param shopId
     * @return
     */
    public Integer selectOnSaleCountByShopId(Long shopId);
    /**
     * 利用状态查总数
     * 上下架的状态
     * @param productQuery
     * @return
     */
    public Integer selectTotalCountWithStatusByPage(ProductQuery productQuery);
	
	public Integer selectCaogaoCountByPage(ProductQuery productQuery);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKeyWithBLOBs(Product record);

    int updateByPrimaryKey(Product record);
}