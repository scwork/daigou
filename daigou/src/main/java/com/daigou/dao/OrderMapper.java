package com.daigou.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.daigou.dto.OrderQuery;
import com.daigou.model.Order;

public interface OrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long id);
    
    List<Order> selectByCondition(OrderQuery qrderQuery);
    
    List<Order> selectCaogaoByShopId(Map<String,Object> paramMap);
    
    public Integer selectTotalCaogaoByShopId(Map<String,Object> paramMap);
    
    public Integer selectTotalCountByCondition(OrderQuery qrderQuery);
    
    public BigDecimal selectTotalAmountByCondition(OrderQuery qrderQuery);
	
	public BigDecimal selectPayAmountByCondition(OrderQuery qrderQuery);
	
	public BigDecimal selectNoPayAmountByCondition(OrderQuery qrderQuery);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
    
    /**
	 * 更新订单那支付金额
	 * @param orderId
	 * @param payAmount
	 * @return
	 */
	public int updatePayAmountByPrimaryKey(Order record);
	
	List<Order> selectOrder4IndexByCondition(OrderQuery qrderQuery);
	
	public int selectOrderCount4ReportByCondition(OrderQuery qrderQuery);
	
	Order selectOrder4ReportByCondition(OrderQuery qrderQuery);
}