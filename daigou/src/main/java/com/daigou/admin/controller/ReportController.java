package com.daigou.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.OrderQuery;
import com.daigou.model.Order;
import com.daigou.model.User;
import com.daigou.service.OrderService;

/**  
 * @Title: UserController.java
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月16日
 */
@Controller("adminReportController")
@RequestMapping("/admin")
public class ReportController{
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/report",method = RequestMethod.GET)
    public String toReportPage(HttpServletRequest request,Model model){
		LOGGER.info("进入报表页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		OrderQuery orderQuery = new OrderQuery();
		//管理员
		if(user.getType() != 1){
			orderQuery.setCreatorId(user.getId());
		}
		orderQuery.setShopId(user.getShopId());
		orderQuery.setTimeType("week");
		orderQuery.builderTime();
		Order orderWeek = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderWeek", orderWeek==null?new Order():orderWeek);
		
		orderQuery.setTimeType("month");
		orderQuery.builderTime();
		Order orderMonth = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderMonth", orderMonth==null?new Order():orderMonth);
		
		orderQuery.setTimeType("season");
		orderQuery.builderTime();
		Order orderSeason = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderSeason", orderSeason==null?new Order():orderSeason);
		
		orderQuery.setTimeType("half");
		orderQuery.builderTime();
		Order orderHalf = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderHalf", orderHalf==null?new Order():orderHalf);
		
		orderQuery.setTimeType("year");
		orderQuery.builderTime();
		Order orderYear = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderYear", orderYear==null?new Order():orderYear);
		
		orderQuery.setTimeType("123");
		orderQuery.builderTime();
		Order orderAll = orderService.selectOrder4ReportByCondition(orderQuery);
		model.addAttribute("orderAll", orderAll==null?new Order():orderAll);
        return "/admin/app/report";
    }
}
