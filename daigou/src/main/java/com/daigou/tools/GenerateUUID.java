package com.daigou.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GenerateUUID {
	private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
	public static Long generateUUID(){
		String timeStr = format.format(new Date());
		//产生1000到9999的随机数
		long temp=Math.round((Math.random()*9+1)*10);
		String idStr = timeStr + temp;
		return Long.parseLong(idStr);
	}
	public static void main(String [] args){
		System.out.println(generateUUID());
	}
	
}
