package com.daigou.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daigou.constants.CommonConstants;
import com.daigou.model.Product;
import com.daigou.model.Shop;
import com.daigou.model.Trip;
import com.daigou.model.User;
import com.daigou.service.ProductService;
import com.daigou.service.ShopService;
import com.daigou.service.TripService;
import com.daigou.service.UserService;

@Controller
public class ShopController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopController.class);
    @Autowired
	private ShopService shopService;
    @Autowired
	private ProductService productService;
    @Autowired
    private UserService userService;
    @Autowired
	private TripService tripService;
	@RequestMapping(value = "/{code}")
    public String toShopPage(HttpServletRequest request,Model model,@PathVariable("code")String code){
		LOGGER.info("进入店铺{}",code.trim());
		if(code == null || "index".equals(code.trim())){
			LOGGER.info("进入店铺列表");
			List<Shop> shopList = shopService.selectAllShop();
			if(shopList != null && shopList.size() > 4){
				model.addAttribute("shop4List", shopList.subList(0, 3));
			}else{
				model.addAttribute("shop4List", shopList);
			}
			model.addAttribute("shopList", shopList);
			return "/shop/app/index";
		}else if("admin".equals(code.trim())){
			LOGGER.info("进入后台首页admin");
			return "/admin/app/index";
		}else{
			LOGGER.info("进入店铺{}",code);
			Shop shop = shopService.selectByCode(code.trim().toLowerCase());
			model.addAttribute("shop", shop);
			List<Trip> tripList = tripService.selectByShopId(shop.getId());
			if(tripList != null && tripList.size() > 0){
				Trip trip = tripList.get(0);
				model.addAttribute("trip", trip);
			}
			//在销售商品数量
			Integer onSaleCount = productService.selectOnSaleCountByShopId(shop.getId());
			model.addAttribute("onSaleCount", onSaleCount);
			//在销售商品列表
			List<Product> productList = productService.selectByShopId(shop.getId(),1, null);
			model.addAttribute("productList", productList);
			Cookie[] cookies = request.getCookies();
			String loginName = "0";
			if (null == cookies) {
				LOGGER.info("没有cookie==============");
			} else {
				//遍历cookie如果找到登录状态则返回true执行原来controller的方法
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(CommonConstants.LOGIN_NAME)) {
						loginName = cookie.getValue();
						LOGGER.info("cookie(loginName)=============="+loginName);
						continue;
					}
				}
			}
			model.addAttribute("loginName", loginName);
			return "/shop/app/shop_index";
		}
    }
	
	@RequestMapping(value = "/{code}/shop-info")
    public String toShopInfoPage(HttpServletRequest request,Model model,@PathVariable("code")String code){
		LOGGER.info("进入店铺{}",code);
		Shop shop = shopService.selectByCode(code.trim().toLowerCase());
		model.addAttribute("shop", shop);
		User user = userService.selectByPrimaryKey(shop.getSalerId());
		model.addAttribute("user", user);
		return "/shop/app/shop-info";
	}
}
