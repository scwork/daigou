package com.daigou.enums;

/**
 * 订单状态
 * add by lizhi 20180410
 * 订单状态：
 * 草稿，新建待支付，支付待确认，
 * 确认待采购，采购代发货，发货待收货，
 * 收货，完毕，退款退货，取消
 */
public enum OrderStatusEnum{
    /**
     * 新创建订单，未支付状态
     */
    NEW_ORDER(1, "新建"),
    /**
     *
     */
    CONFIRM_PACKAGING(2, "备货中"),
    /**
     *
     */
    SENDED(3, "已发货"),
    /**
     *
     */
    RECEIVED(4, "已收货"),
    /**
     *
     */
    OVER(5, "完毕"),
    /**
     *
     */
    REFOUND(6, "退款退货"),
    /**
     *
     */
    CANCEL(7, "取消");

    private int code;
    private String text;

    /**
     * 私有构造函数，防止外面实例化
     * @param code
     * @param text
     */
    private OrderStatusEnum(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    /**
     * 订单状态
     *
     * @param code
     * @return
     */
    public static OrderStatusEnum valueOf(int code) {
        for (OrderStatusEnum repayStatusEnum : values()) {
            if (code == repayStatusEnum.getCode()) {
                return repayStatusEnum;
            }
        }
        return OVER;
    }
}
