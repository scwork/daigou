package com.daigou.enums;

/**
 * trip状态
 * add by lizhi 20180410
 * trip状态：
 * 准备，出发，采购中，返程，结束
 */
public enum TripStatusEnum{
    /**
     * 新创建订单，未支付状态
     */
    PREPARE(1, "准备"),
    /**
     *
     */
    GO_GO(2, "接单中"),
    /**
     *
     */
    PURCHASEING(3, "采购中"),
    /**
     *
     */
    BACK_BACK(4, "返程"),
    /**
     *
     */
    OVER(5, "结束");

    private int code;
    private String text;

    /**
     * 私有构造函数，防止外面实例化
     * @param code
     * @param text
     */
    private TripStatusEnum(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    /**
     * 订单状态
     *
     * @param code
     * @return
     */
    public static TripStatusEnum valueOf(int code) {
        for (TripStatusEnum repayStatusEnum : values()) {
            if (code == repayStatusEnum.getCode()) {
                return repayStatusEnum;
            }
        }
        return OVER;
    }
}
