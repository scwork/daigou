//表单验证
$(function(){
	$("#startDate").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language:"zh-CN",
        format: "yyyy-mm-dd"
    });
	$("#endDate").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language:"zh-CN",
        format: "yyyy-mm-dd"
    });
	function updateTrip(){
		//获取form的值
		var id = $('#id').val();
		var targetAddress = $('#targetAddress').val();
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		var status = $('#status').val();
		var tripPicImage = $('#tripPicImage').attr("src");
		var tripPicImage1 = $('#tripPicImage1').attr("src");
		var tripPicImage2 = $('#tripPicImage2').attr("src");
		var tripPicImage3 = $('#tripPicImage3').attr("src");
		var tripPicImage4 = $('#tripPicImage4').attr("src");
		var tripPicImage5 = $('#tripPicImage5').attr("src");
		var tripPicImage6 = $('#tripPicImage6').attr("src");
		var remark = $('#remark').val();
		//是不是草稿，下单也必须有商品name
		if(targetAddress == null || targetAddress == ""){
			layer.msg("请输入行程目的地!",{icon:2,time:2000});
			return;
		}
		//
		if(startDate == null || startDate == ""){
			layer.msg("请输入出发时间!",{icon:2,time:2000});
			return;
		}
		
		if(endDate == null || endDate == ""){
			layer.msg("请输入返程时间!",{icon:2,time:2000});
			return;
		}
		if(tripPicImage == null || tripPicImage == ""){
			layer.msg("行程主图片不能为空!",{icon:2,time:2000});
			return;
		}
		if(tripPicImage != null && tripPicImage.length > 512){
			layer.msg("行程主图片url不能超过512位!",{icon:2,time:2000});
			return;
		}
		if(remark != null && remark.length > 512){
			layer.msg("行程描述不能超过512位!",{icon:2,time:2000});
			return;
		}
		
		var trip = {};
		trip["id"] = id;
		trip["targetAddress"] = targetAddress;
		trip["startDate"] = startDate;
		trip["endDate"] = endDate;
		trip["status"] = status;
		trip["tripPic"] = tripPicImage;
		trip["tripPic1"] = tripPicImage1;
		trip["tripPic2"] = tripPicImage2;
		trip["tripPic3"] = tripPicImage3;
		trip["tripPic4"] = tripPicImage4;
		trip["tripPic5"] = tripPicImage5;
		trip["tripPic6"] = tripPicImage6;
		trip["remark"] = remark;
		$.ajax({
			url:baselocation+'/trip',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: JSON.stringify(trip),
			success:function(result){
				//取消冻结
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					window.location.href=baselocation+'/go-go';
				}
			},
			error:function(result){
				//取消冻结
				alert("系统异常,请联系管理员！");
			}
		});
	}
	$('#saveTripBtn').click(function () {
		updateTrip();
	})
	
	function updateAdvert(){
		//获取form的值
		var id = $('#id').val();
		var targetAddress = $('#targetAddress').val();
		var startDate = $('#startDate').val();
		var endDate = $('#endDate').val();
		var status = $('#status').val();
		var tripPicImage = $('#tripPicImage').attr("src");
		var tripPicImage1 = $('#tripPicImage1').attr("src");
		var tripPicImage2 = $('#tripPicImage2').attr("src");
		var tripPicImage3 = $('#tripPicImage3').attr("src");
		var tripPicImage4 = $('#tripPicImage4').attr("src");
		var tripPicImage5 = $('#tripPicImage5').attr("src");
		var tripPicImage6 = $('#tripPicImage6').attr("src");
		var remark = $('#remark').val();
		//是不是草稿，下单也必须有商品name
		if(targetAddress == null || targetAddress == ""){
			layer.msg("请输入广告标题!",{icon:2,time:2000});
			return;
		}
		//
		if(startDate == null || startDate == ""){
			layer.msg("请输入广告时间(起)!",{icon:2,time:2000});
			return;
		}
		
		if(endDate == null || endDate == ""){
			layer.msg("请输入广告时间(止)!",{icon:2,time:2000});
			return;
		}
		if(tripPicImage == null || tripPicImage == ""){
			layer.msg("广告主图片不能为空!",{icon:2,time:2000});
			return;
		}
		if(tripPicImage != null && tripPicImage.length > 512){
			layer.msg("广告主图片url不能超过512位!",{icon:2,time:2000});
			return;
		}
		if(remark != null && remark.length > 512){
			layer.msg("广告描述不能超过512位!",{icon:2,time:2000});
			return;
		}
		
		var trip = {};
		trip["id"] = id;
		trip["targetAddress"] = targetAddress;
		trip["startDate"] = startDate;
		trip["endDate"] = endDate;
		trip["status"] = status;
		trip["tripPic"] = tripPicImage;
		trip["tripPic1"] = tripPicImage1;
		trip["tripPic2"] = tripPicImage2;
		trip["tripPic3"] = tripPicImage3;
		trip["tripPic4"] = tripPicImage4;
		trip["tripPic5"] = tripPicImage5;
		trip["tripPic6"] = tripPicImage6;
		trip["remark"] = remark;
		$.ajax({
			url:baselocation+'/trip',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: JSON.stringify(trip),
			success:function(result){
				//取消冻结
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					window.location.href=baselocation+'/go-go';
				}
			},
			error:function(result){
				//取消冻结
				alert("系统异常,请联系管理员！");
			}
		});
	}
	$('#saveAdvertBtn').click(function () {
		updateAdvert();
	})
});