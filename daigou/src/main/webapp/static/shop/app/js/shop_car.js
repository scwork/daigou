//页面加载完毕后执行获取用户信息
$(function(){
	// 数量减
	$(".minus").click(function() {
		var t = $(this).parent().find('.num');
		t.text(parseInt(t.text()) - 1);
		if (t.text() <= 1) {
			t.text(1);
		}
		totalPrice(t.text());
	});
	// 数量加
	$(".plus").click(function() {
		var t = $(this).parent().find('.num');
		t.text(parseInt(t.text()) + 1);
		if (t.text() <= 1) {
			t.text(1);
		}
		totalPrice(t.text());
	});
	function totalPrice(num){
		var price = $("#price").val();
		var totalAmount = (price*parseInt(num)).toFixed(2);
		$("#span-total-amount").html("￥"+totalAmount);
		$("#i-all-total-amount").html("￥"+totalAmount);
	}
})