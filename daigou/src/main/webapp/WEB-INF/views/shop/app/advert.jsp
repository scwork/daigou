<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/shop/app/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	<meta name="format-detection" content="telephone=no, email=no"/>
	<meta charset="UTF-8">
	<title>${trip.targetAddress }</title>
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/core.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/icon.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/home.css">
	<link rel="stylesheet" href="<%=basePath%>/static/shop/app/css/share.css">
	<link rel="shortcut icon" href="<%=basePath%>/favicon.ico" type="image/x-icon" />
	<link href="<%=basePath%>/favicon.ico" sizes="114x114" rel="apple-touch-icon-precomposed">
	<style>
		.m-button {
			padding: 0 0.24rem;
		}

		.btn-block {
			text-align: center;
			position: relative;
			border: none;
			pointer-events: auto;
			width: 100%;
			display: block;
			font-size: 1rem;
			height: 2.5rem;
			line-height: 2.5rem;
			margin-top: 0.5rem;
			border-radius: 3px;
		}

		.btn-primary {
			background-color: #04BE02;
			color: #FFF;
		}

		.btn-warning {
			background-color: #FFB400;
			color: #FFF;
		}
		.mask-black {
			background-color: rgba(0, 0, 0, 0.6);
			position: fixed;
			bottom: 0;
			right: 0;
			left: 0;
			top: 0;
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
			z-index:999;
		}

		.m-actionsheet {
			text-align: center;
			position: fixed;
			bottom: 0;
			left: 0;
			width: 100%;
			z-index: 1000;
			background-color: #EFEFF4;
			-webkit-transform: translate(0, 100%);
			transform: translate(0, 100%);
			-webkit-transition: -webkit-transform .3s;
			transition: -webkit-transform .3s;
			transition: transform .3s;
			transition: transform .3s, -webkit-transform .3s;
		}
		.actionsheet-toggle {
			-webkit-transform: translate(0, 0);
			transform: translate(0, 0);
		}
		.actionsheet-item {
			display: block;
			position: relative;
			font-size: 0.8rem;
			color: #555;
			height: 2rem;
			line-height: 2rem;
			background-color: #FFF;
		}
		.actionsheet-item:after {
			content: '';
			position: absolute;
			z-index: 2;
			bottom: 0;
			left: 0;
			width: 100%;
			height: 1px;
			border-bottom: 1px solid #D9D9D9;
			-webkit-transform: scaleY(0.5);
			transform: scaleY(0.5);
			-webkit-transform-origin: 0 100%;
			transform-origin: 0 100%;
		}
		.actionsheet-action {
			display: block;
			margin-top: .15rem;
			font-size: 0.8rem;
			color: #555;
			height:3rem;
			line-height: 3rem;
			background-color: #FFF;
			position:absolute;
			top:10px;
			right:0;
		}

	</style>
</head>
<body>
	<header class="aui-header-default aui-header-fixed aui-header-bg">
		<a href="javascript:history.back(-1)" class="aui-header-item">
			<i class="aui-icon aui-icon-back-white"></i>
		</a>
		<div class="aui-header-center aui-header-center-clear">
			<div class="aui-header-center-logo">
				<div id="scrollAdvertPro">
					<span class="current">${trip.targetAddress }</span>
				</div>
			</div>
		</div>
		<a href="javascript:;" class="aui-header-item-icon select"    style="min-width:0;">
			<i class="aui-icon aui-icon-share-pd selectVal" onselectstart="return false"></i>
			<div class="aui-header-drop-down selectList">
				<div class="aui-header-drop-down-san"></div>
				<div class="">
					<p class="" onclick="location='<%=basePath%>/${shop.code }'">首页</p>
					<p class="" onclick="location='index.html'">帮助</p>
					<p class="" id="share_btn_p">分享</p>
				</div>
			</div>
		</a>
	</header>
	<div class="aui-banner-content aui-fixed-top">
	</div>
	<div class="aui-product-content">
		<div class="aui-dri"></div>
		<div class="aui-product-evaluate">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">${trip.targetAddress }</div>
			</a>
		</div>
		<div class="aui-product-pages">
			<div class="aui-product-pages-title">
			</div>
			<div class="aui-product-pages-img">
				<c:if test="${not empty trip.tripPic }">
				<img src="${trip.tripPic }" alt="">
				</c:if>
			</div>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-evaluate">
			<a href="<%=basePath%>/${shop.code }" class="aui-address-cell aui-fl-arrow aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">
					<div class="clearfix">
						<div class="aui-product-shop-img">
							<div class="shop_logo_contain">
								<img id="shop_logo_" src="${shop.logo }" alt=""/>
							</div>
						</div>
						<div class="aui-product-shop-text">
							<h3>${shop.name }</h3>
							<p>精选商家  服务保障</p>
							<p>在售商品${onSaleCount }件</p>
						</div>
					</div>
				</div>
				<div class="aui-address-cell-ft">
					<span>进店看看</span>
				</div>
			</a>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-evaluate">
			<a href="#" class="aui-address-cell aui-fl-arrow-clear">
				<div class="aui-address-cell-bd">${trip.remark }</div>
			</a>
		</div>
		<div class="aui-dri"></div>
		<div class="aui-product-pages">
			<div class="aui-product-pages-title">
			</div>
			<div class="aui-product-pages-img">
				<c:if test="${not empty trip.tripPic1 }">
				<img src="${trip.tripPic1 }" alt="">
				</c:if>
				<c:if test="${not empty trip.tripPic2 }">
				<img src="${trip.tripPic2 }" alt="">
				</c:if>
				<c:if test="${not empty trip.tripPic3 }">
				<img src="${trip.tripPic3 }" alt="">
				</c:if>
				<c:if test="${not empty trip.tripPic4 }">
				<img src="${trip.tripPic4 }" alt="">
				</c:if>
				<c:if test="${not empty trip.tripPic5 }">
				<img src="${trip.tripPic5 }" alt="">
				</c:if>
				<c:if test="${not empty trip.tripPic6 }">
				<img src="${trip.tripPic6 }" alt="">
				</c:if>
			</div>
		</div>
		<div class="aui-product-pages">
			<div class="aui-product-pages-title">长按二维码保存到本地!赶快分享吧！
			</div>
			<div id="advertQrCodeDiv" class="aui-product-pages-img">
			</div>
			<div style="position: relative;margin:0 auto;padding:1px 1px;border:1px solid;width:204px;height:204px;">
				<img id="advertQrCodeImage" alt="">
			</div>
		</div>
	</div>
	<input id="tripId" type="hidden" value="${trip.id }">
	<%@ include file="share.jsp" %>
	<script src="<%=basePath%>/static/shop/app/js/jquery.min.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui.js"></script>
	<script src="<%=basePath%>/static/shop/app/js/aui-down.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/share.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/my_pic_view.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/app/js/jquery.qrcode.min.js"></script>
	<script type="text/javascript">
	window.onload=function() {
	    show_pic_view('65','65','shop_logo_');
	}
	</script>
	<script type="text/javascript">
        $(function () {
        	var tripId = $('#tripId').val();
        	var qrcode = $('#advertQrCodeDiv').qrcode({width:200,height:200,correctLevel:0,text:'<%=basePath%>/trip/'+tripId}).hide();
        	var canvas=qrcode.find('canvas').get(0);  
        	$('#advertQrCodeImage').attr('src',canvas.toDataURL('image/jpg'));
            //绑定滚动条事件
            $(window).bind("scroll", function () {
                var sTop = $(window).scrollTop();
                var sTop = parseInt(sTop);
                if (sTop >= 100) {
                    if (!$("#scrollAdvertPro").is(":visible")) {
                        try {
                            $("#scrollAdvertPro").slideDown();
                        } catch (e) {
                            $("#scrollAdvertPro").show();
                        }
                    }
                }
                else {
                    if ($("#scrollAdvertPro").is(":visible")) {
                        try {
                            $("#scrollAdvertPro").slideUp();
                        } catch (e) {
                            $("#scrollAdvertPro").hide();
                        }
                    }
                }
            });
        })
	</script>
</body>
</html>