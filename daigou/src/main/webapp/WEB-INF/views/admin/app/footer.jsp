<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="am-menu">
    <div class="am-menu-type line-top">
        <div class="am-menu-column suo-pei"  id="suo-pei">
            <a href="<%=basePath%>/admin/order">
                <i class="iconfont"></i>
                <span>订单</span>
            </a>
        </div>
        <div class="am-menu-column cao-gao"  id="cao-gao">
            <a href="<%=basePath%>/admin/product">
                <i class="iconfont"></i>
                <span>商品</span>
            </a>
        </div>
        <div class="am-menu-column index"  id="index">
            <a href="<%=basePath%>/admin/index">
                <i class="iconfont"></i>
                <span>首页</span>
            </a>
        </div>
        <div class="am-menu-column news"  id="news">
            <a href="<%=basePath%>/admin/go-go">
                <i class="iconfont"></i>
                <span>GO!!!</span>
            </a>
        </div>
        <div class="am-menu-column account"  id="account">
            <a href="<%=basePath%>/admin/account">
                <i class="iconfont"></i>
                <span>账户</span>
            </a>
        </div>
    </div>
</div>