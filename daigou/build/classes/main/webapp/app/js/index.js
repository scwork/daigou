$(function() {
    App.init();

    var Index = (function() {
        var me = {};

        // 处理一级菜单点击
//        me.handleMenuClick = function() {
//            $('#page-sidebar-menu > li').click(function(e) {
//                var menu = $('#page-sidebar-menu');
//                var li = menu.find('li.active').removeClass('active');
//            });
//        };

        // 处理子菜单点击
        me.handleSubMenuClick = function() {
            $('.dropdown-menu li a').click(function(e) {
                e.preventDefault();
                var url = this.href;
                if (url != null && url != 'javascript:;') {
                	if(url.indexOf('logout')<0){
                		$.get(url, function(data) {
                			$('#main-content').html(data);
                		});
                	}
                }
            });
        };

        me.init = function() {
//            me.handleMenuClick();
            me.handleSubMenuClick();
        };

        return me;
    })();

    Index.init();

    $('#btn-dashboard').trigger("click");
});